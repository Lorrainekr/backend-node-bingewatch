
const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});
app.use(cors({
    origin: ['*'],
    "methods": "GET,PUT,POST",
    "preflightContinue": true,
    "optionsSuccessStatus": 200,
    credentials: true
}));
const PORT = process.env.PORT || 8080;
/**
 * Import MongoClient & connexion à la DB
 */
const MongoClient = require('mongodb').MongoClient;
const urlocal = 'mongodb://localhost:27017';
const url = process.env.URLMONGO || urlocal;
//const client = new MongoClient(process.env.urlMONGO);
const dbName = 'TVShowlist';
let db;
MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");
    db = client.db(dbName);
});
app.use(express.json())
app.set("port", PORT);
app.get("/", (req, res) => {
    res.send("Is running")
})
app.get('/TVShowlist', async (req,res) => {
    try {
        const docs = await db.collection('TVShowlist').find({}).toArray()
        res.status(200).json(docs)
    } catch (err) {
        console.log(err)
        throw err
    }
})

app.get('/TVShowlist/:id', async (req,res) => {
    let id =parseInt(req.params.id);
    try {
        const docs = await db.collection('TVShowlist').find({"id": id }).toArray();
        res.status(200).json(docs)
    } catch (err) {
        console.log(err)
        throw err
    }
})

app.get('/TVShowlist/name/:name', async (req,res) => {
    let name= req.params.name;
    console.log(name);
    try {
        const docs = await db.collection('TVShowlist').find({"name": name}).toArray();
        res.status(200).json(docs)
    } catch (err) {
        console.log(err)
        throw err
    }
})


app.listen(process.env.PORT || 8081, () => {
    console.log(`Server is running on port ${PORT}.`);
})
